import { Component, OnInit, OnDestroy,Input, Output, EventEmitter,OnChanges, SimpleChanges, SimpleChange  } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { Coordinate  } from "../../shared/models/coordinate.model";
import { FlightPlan  } from "../../shared/models/flight-plan.model";

import { CurrentFlightPlanService } from "../../shared/services/current-flight-plan.service";

@Component({
  selector: 'home-map',
  templateUrl: './home-map.component.html',
  styleUrls: ['./home-map.component.scss']
})
export class HomeMapComponent implements OnChanges, OnInit, OnDestroy {
  
  /**  MAP  *****************************/
  title: string = 'Flight Plan';
  userLat: number = 46.519056;
  userLng: number = 6.566543;

  flightPlan : FlightPlan = new FlightPlan("Nouveau plan",[]);

  private _subscriptions: Subscription[] = [];

  /******* VARIOUS ***********/
  @Input() creationMode: boolean;

  constructor(
    private currentFightPlanService: CurrentFlightPlanService,
  ) { }

  ngOnInit() {

    this._subscriptions.push(this.currentFightPlanService.flightPlan.subscribe(flightPlan => {
      this.flightPlan = flightPlan;
    }));

    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.userLat = position.coords.latitude;
        this.userLng = position.coords.longitude;
      });
    }
  }
  // We reset the coordinates when the user changes the creationMode
  ngOnChanges(changes: SimpleChanges) {
    if(changes.creationMode.currentValue == false){
      this.flightPlan.Coordinates = [];
      this.currentFightPlanService.set(this.flightPlan);
    }
  }

  ngOnDestroy(){
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  addCoordinates(e){
    // Stop clicks when not in creationMode
    if(!this.creationMode){return;}
    let coordinates = new Coordinate(e.coords.lat, e.coords.lng);
    this.flightPlan.Coordinates.push(coordinates);
    this.currentFightPlanService.set(this.flightPlan);
  }

}
