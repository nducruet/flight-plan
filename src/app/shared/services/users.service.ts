import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map'
import { MessagesService } from '../../shared/services/messages.service';
import { User }           from '../../shared/models/user.model';

@Injectable()
export class UsersService {
  
    public user: User;

    text_error : string;
    text_success : string;
    text_warning : string;

    constructor(
        private messagesService: MessagesService,
    ) {}

    get() {
    }

    sendMessage(message): void {
        this.messagesService.sendMessage(message);
    }

    clearMessage(): void {
        this.messagesService.clearMessage();
    }
}


  
