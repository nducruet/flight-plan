import { Injectable } from '@angular/core';
import { Headers, RequestOptions,ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/map';

@Injectable()
export class HeadersService {

    token:string;
  
    constructor(
        ) {
    }

    getJSONNoAuth(){
        return new RequestOptions({ headers: new Headers({
            'Content-Type': 'application/json',
          }) });
    }

    getWithAuth(){
        return new RequestOptions({ headers: new Headers() });
    }

    getPicWithAuth(){
        return new RequestOptions({headers: new Headers(),
            responseType: ResponseContentType.Blob 
        });
    }

    getJSONWithAuth(){
        return new RequestOptions({ headers: new Headers({
            'Content-Type': 'application/json',
          }) });
    }

    postJSONNoAuth(){
        return new RequestOptions({ headers: new Headers({
            'Content-Type': 'application/json',
          })});
    }

    postJSONWithAuth(){
        return new RequestOptions({ headers: new Headers({
            'Content-Type': 'application/json',
          }) });
    }
}