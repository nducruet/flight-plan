import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';  

/*******************************************************************************************************
  CUSTOM MODULES
****************************************************************************************************************** */
// import { FilesModule } from './analysis/analysis.module';
import { HomeModule } from './home/home.module';
import { NotFoundModule } from './not-found/not-found.module';

/*******************************************************************************************************
  COMPONENTS
****************************************************************************************************************** */
import { HomeComponent } from './home/home.component';
import { NotFoundComponent } from './not-found/not-found.component';

/*************************************************************************************************************
  RESOLVERS
***************************************************************************************************** */

import { FetchFlightPlansService } from './shared/resolvers/fetch-flight-plans.service';

const appRoutes: Routes = [
  { path: '',                                 component: HomeComponent,   resolve: { flightPlans: FetchFlightPlansService}           },
  { path: '**',                               component: NotFoundComponent }
];


@NgModule({
  imports: [
    CommonModule,
    // FilesModule,
    NotFoundModule,
    HomeModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [
    HomeComponent,
    NotFoundComponent,
  ],
  providers:[
    FetchFlightPlansService
  ],
  exports: [
    RouterModule
  ],
})
export class AppRoutingModule { }