import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Rx';

import { FlightPlan  } from "../../shared/models/flight-plan.model";

@Injectable()
export class CurrentFlightPlanService {

  flightPlan: Subject<FlightPlan> = new Subject<FlightPlan>();

  constructor() {
  }

  set(flightPlan) {
    this.flightPlan.next(flightPlan);
  }

}
