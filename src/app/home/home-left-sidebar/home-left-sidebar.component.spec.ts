import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeLeftSidebarComponent } from './home-left-sidebar.component';

describe('HomeLeftSidebarComponent', () => {
  let component: HomeLeftSidebarComponent;
  let fixture: ComponentFixture<HomeLeftSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeLeftSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeLeftSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
