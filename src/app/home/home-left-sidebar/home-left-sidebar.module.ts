import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeLeftSidebarComponent } from './home-left-sidebar.component';

import {
  MatButtonModule,
  MatMenuModule,
  MatToolbarModule,
  MatIconModule,
  MatCardModule,
  MatFormFieldModule,
  MatListModule,
} from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonModule,
    MatMenuModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatFormFieldModule,
    MatListModule,
  ],
  declarations: [
    HomeLeftSidebarComponent
  ],
  exports:[
    HomeLeftSidebarComponent,
  ]
})
export class HomeLeftSidebarModule { }
