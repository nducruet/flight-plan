import { TestBed, inject } from '@angular/core/testing';

import { FlightPlansService } from './flight-plans.service';

describe('FlightPlansService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FlightPlansService]
    });
  });

  it('should be created', inject([FlightPlansService], (service: FlightPlansService) => {
    expect(service).toBeTruthy();
  }));
});
