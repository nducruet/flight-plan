import {Coordinate} from './coordinate.model';

export class FlightPlan {
    constructor(
        public Name: string,
        public Coordinates: Array<Coordinate>,
    ){}
}