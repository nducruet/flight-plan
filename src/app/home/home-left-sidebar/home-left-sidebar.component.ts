import { Component, OnInit, OnDestroy,Input, Output, EventEmitter  } from '@angular/core';

import { FlightPlan  } from "../../shared/models/flight-plan.model";
import { CurrentFlightPlanService } from "../../shared/services/current-flight-plan.service";

@Component({
  selector: 'home-left-sidebar',
  templateUrl: 'home-left-sidebar.component.html',
  styleUrls: ['home-left-sidebar.component.scss']
})
export class HomeLeftSidebarComponent implements OnInit, OnDestroy {

  @Input() flightPlans: FlightPlan[];

  @Output() emitter: EventEmitter<boolean> = new EventEmitter();

  constructor(
    private currentFightPlanService: CurrentFlightPlanService,
  ) { }

  ngOnInit() {
  }

  ngOnDestroy(){
    
  }

  editFlightPlan(index){
    this.emitter.emit(true);
    this.currentFightPlanService.set(this.flightPlans[index]);
  }

}
