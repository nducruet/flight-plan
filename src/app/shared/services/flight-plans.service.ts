import { Injectable } from '@angular/core';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Observable } from 'rxjs/Rx';

import { environment } from '../../../environments/environment.prod';

import { NotificationsService } from 'angular2-notifications';

import { FlightPlan } from '../models/flight-plan.model';

@Injectable()
export class FlightPlansService {

	flightPlans: FlightPlan[];
	error_message: string;
	constructor(
		private notificationsService: NotificationsService,
	) { }

	public getList(): FlightPlan[] {
		return JSON.parse(localStorage.getItem('flightPlans'));
	}

	public obsList(): Observable<FlightPlan[]> {
		let currentPlans = this.getList();
		if(!currentPlans){
			currentPlans = [];
		} else{
			currentPlans.map(x => x as FlightPlan);
		}
		return Observable.of(currentPlans);
	}

	public save(plan:FlightPlan){
		let currentPlans = JSON.parse(localStorage.getItem('flightPlans'));
		if(!currentPlans){
			currentPlans = [plan];
		} else{
			let existingPlan = false;
			currentPlans.forEach(function(storedPlan, idx){
				if(storedPlan.Name == plan.Name){
					storedPlan.Coordinates = plan.Coordinates;
					existingPlan = true;
				}
				// Add new on last iteration
				if (existingPlan == false && idx === currentPlans.length - 1){
					currentPlans.push(plan);
				}
			 });
		}
		localStorage.setItem('flightPlans', JSON.stringify(currentPlans));
		return true;
	}

	public get(name:string): Observable<FlightPlan> {
		let currentPlans = JSON.parse(localStorage.getItem('flightPlans'));
		return currentPlans.filter(plan => plan.Name == name)[0];
	}

	private handleError(error: any): Observable<any> {
		var type_error  = error.status.toString()[0];
		this.error_message = type_error != 5 ? error._body : environment.msgErrors.server;
		this.notificationsService.error(
			'&nbsp;',
			this.error_message,
			{
				timeOut: 3000,
				pauseOnHover: false,
				clickToClose: false,
			}
		);
		return Observable.throw(new Error(error.status));
	}

}
