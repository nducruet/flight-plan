import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { FlightPlansService } from '../services/flight-plans.service';

import { FlightPlan } from '../models/flight-plan.model';

@Injectable()
export class FetchFlightPlansService {

	constructor(
		private flightPlansService: FlightPlansService,
	) { }

	resolve(route: ActivatedRouteSnapshot): Observable<FlightPlan[]> | boolean {
		// var params = route.params;
		return this.flightPlansService.obsList();
	}
}
