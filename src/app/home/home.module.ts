import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';

import {
  MatSidenavModule,
  MatButtonModule,
  
} from '@angular/material';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

/*******************************************************************************************************
  CUSTOM MODULES
****************************************************************************************************************** */
import { HomeLeftSidebarModule } from './home-left-sidebar/home-left-sidebar.module';
import { HomeRightSidebarModule } from './home-right-sidebar/home-right-sidebar.module';
import { HomeMapModule } from './home-map/home-map.module';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  imports: [
    BrowserModule,
    CommonModule,
    HomeLeftSidebarModule,
    HomeMapModule,
    SharedModule,
    MatSidenavModule,
    FontAwesomeModule
  ],
  declarations: [
  ],
  exports:[
    HomeLeftSidebarModule,
    HomeRightSidebarModule,
    HomeMapModule,
    MatSidenavModule,
    MatButtonModule,
    FontAwesomeModule
  ],
  providers: [],
})
export class HomeModule { }
