import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgmCoreModule } from '@agm/core';

import { HomeMapComponent } from './home-map.component';

import {MatButtonModule} from '@angular/material';

import {MatCheckboxModule} from '@angular/material/checkbox';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatCheckboxModule,
    MatButtonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCPbw2_A4gk721Qs2cXc5loJR9zQdDuLbs'
    }),
  ],
  declarations: [
    HomeMapComponent
  ],
  exports:[
    HomeMapComponent,
  ]
})
export class HomeMapModule { }
