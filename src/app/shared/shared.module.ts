import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

/*************************************************************************************************************
  SERVICES
***************************************************************************************************** */
import { UsersService } from './services/users.service';
import { CurrentFlightPlanService } from './services/current-flight-plan.service';
import { FlightPlansService } from './services/flight-plans.service';
import { MessagesService } from './services/messages.service';
import { HeadersService } from './services/headers.service';
import { NotificationsService } from 'angular2-notifications';

@NgModule({
  imports: [
    CommonModule
  ],
  providers:[
    UsersService,
    FlightPlansService,
    CurrentFlightPlanService,
    MessagesService,
    HeadersService,
    NotificationsService,
  ],
  declarations: [],
  exports: [
  ],
})
export class SharedModule { }
