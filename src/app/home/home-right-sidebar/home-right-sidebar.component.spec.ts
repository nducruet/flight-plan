import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeRightSidebarComponent } from './home-right-sidebar.component';

describe('HomeRightSidebarComponent', () => {
  let component: HomeRightSidebarComponent;
  let fixture: ComponentFixture<HomeRightSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeRightSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeRightSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
