import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';

import { MatSidenav } from "@angular/material";

import { ActivatedRoute, Params } from '@angular/router';

import { FlightPlan  } from "../shared/models/flight-plan.model";

import { faPlus } from '@fortawesome/free-solid-svg-icons';

import { FlightPlansService } from "../shared/services/flight-plans.service";

import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {

  opened: boolean = true;

  creationMode : boolean = false;

  @ViewChild('leftSideNav') leftSideNavRef: MatSidenav;
  @ViewChild('rightSideNav') rightSideNavRef: MatSidenav;
  
  faPlus = faPlus;

  flightPlans: FlightPlan[] = [];

  private _subscription: Subscription;

  constructor(
    private route: ActivatedRoute,
    private flightPlansService: FlightPlansService,
  ) { }

  ngOnInit() {

    this.route.data
      .subscribe((data: { flightPlans: FlightPlan[] }) => {
        this.flightPlans = data.flightPlans;
      });
  }

  ngOnDestroy(){
    this._subscription.unsubscribe();
  }

  triggerCreationMode(){
    this.rightSideNavRef.toggle();
    this.creationMode = true;
  }

  changeCreationMode(e){
    this.creationMode = e;
    if(e==true){
      this.rightSideNavRef.open();
      this.leftSideNavRef.close();
    } else{
      this.rightSideNavRef.close();
      this.leftSideNavRef.open();
    }
  }

  refreshFlightPlansList(){
    console.info("refrsshing list");
    this.flightPlans = this.flightPlansService.getList();
  }

}
