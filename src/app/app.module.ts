import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { SimpleNotificationsModule } from 'angular2-notifications';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

/*******************************************************************************************************
  CUSTOM MODULES
****************************************************************************************************************** */
import { SharedModule } from './shared/shared.module';
import { AppRoutingModule } from './app-routings';

/*************************************************************************************************************
  PIPES
***************************************************************************************************** */
import { DatePipe } from '@angular/common';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    HttpModule,
    HttpClientModule,
    SimpleNotificationsModule.forRoot(),
    NgbModule.forRoot(),
    SharedModule,
    AppRoutingModule,
  ],
  exports:[
    
  ],
  providers: [
    DatePipe,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
