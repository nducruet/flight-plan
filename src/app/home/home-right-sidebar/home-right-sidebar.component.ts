import { Component, OnInit, OnDestroy,Input, Output, EventEmitter  } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';

import { CurrentFlightPlanService } from "../../shared/services/current-flight-plan.service";
import { FlightPlansService } from "../../shared/services/flight-plans.service";

import { FlightPlan  } from "../../shared/models/flight-plan.model";

import { faTrash } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'home-right-sidebar',
  templateUrl: 'home-right-sidebar.component.html',
  styleUrls: ['home-right-sidebar.component.scss']
})
export class HomeRightSidebarComponent implements OnInit, OnDestroy {

  @Output() emitter: EventEmitter<boolean> = new EventEmitter();
  @Output() emitter2: EventEmitter<boolean> = new EventEmitter();

  private _subscriptions: Subscription[] = [];

  flightPlan : FlightPlan = new FlightPlan("Nouveau plan",[]);

  faTrash = faTrash;

  constructor(
    private currentFightPlanService: CurrentFlightPlanService,
    private flightPlansService: FlightPlansService,
  ) { }

  ngOnInit() {
    this._subscriptions.push(this.currentFightPlanService.flightPlan.subscribe(flightPlan => {
      this.flightPlan = flightPlan;
    }));
  }

  ngOnDestroy(){
    this._subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  cancelCoordinates(){
    this.emitter.emit(false);
  }

  saveCoordinates(){
    // Creation Mode set to false
    this.emitter.emit(false);
    this.flightPlansService.save(this.flightPlan);
    // Refresh list of stored flight plans
    this.emitter2.emit(true);
  }

  deleteCoordinate(index){
    this.flightPlan.Coordinates.splice(index,1);
    this.currentFightPlanService.set(this.flightPlan);

  }

}
