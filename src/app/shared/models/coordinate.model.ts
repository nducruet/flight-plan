export class Coordinate {
    constructor(
        public Lat: number,
        public Lng: number,
    ){}
}